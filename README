GimpSketchbook
==============

General
-------

A set of python scripts to simplify doodling in Gimp while using your Tablet PC
in tablet mode, with no access to the keyboard (in other words, reduce to zero
the need of the keyboard).

The scripts consists into a Gimp plugin that registers a D-Bus server and a GUI
app that gives you access to some useful actions via a buttons (so no need to
do those via the keyboard).

Prerequisites
-------------

1. wacom-tools:
   $ sudo apt-get install wacom-tools
2. dogtail:
   $ sudo apt-get install python-dogtail
3. activate aspi in Gnome (full gimpsketchbook functionality is not available
   under KDE):

   Access this menu: "applications->preferences->accessibility->assistive
   technologies preferences" and select the "enable assistive technologies"
   checkbox.

   Log off and back on to activate the changes.

Installation
------------

1. add src/gimp dir to Gimp's plugin dirs
2. run sketchbook.py from src/client

Usage
-----

Upon restart, the sketchbook will try to reopen the last file used and start
automatically gimp. Only the first time you the app you must follow these
steps:

1. Press the "new" button to select a new name for your sketchbook or select an
   existing sketchbook. Sketchbooks are by default XCF files, but any file that
   supports layers is probably ok.
2. If the file is new, you can use the "width" and "height" spins to set the
   size of the sketchbook
3. Press the "start gimp" to start gimp

Screen rotation
---------------

Screen rotation uses xrandr and xsetwacom. It will probably work only on Linux.

Platform support
----------------

Tested only on Debian and Ubuntu.
