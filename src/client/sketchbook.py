#!/usr/bin/python

# GIMP Sketchbook - Transform GIMP into a sketchbook
# Copyright (C) 2009 Ionutz Borcoman, Tina Russell
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pygtk
pygtk.require("2.0")
import gtk

import subprocess
import socket
import sys
import struct
import logging
import ConfigParser
import os
import datetime
import time
import dbus
import traceback
from optparse import OptionParser

try:
	import dogtail.tree
	HAS_DOGTAIL = True
except:
	HAS_DOGTAIL = False

# Ini values
INI_DIR =  os.path.expanduser('~/.gimpsketchbook')
INI_FILE = os.path.join(INI_DIR, 'gimpsketchbookrc')

INI_SERVER = 'script-fu-server'
INI_SERVER_PORT = 'port'
INI_SERVER_PORT_DEFAULT = '10008'
INI_SERVER_LOG = 'log'
INI_SERVER_LOG_DEFAULT = '/tmp/gimpsketchbook.log'

INI_LOGGING = 'logging'
INI_LOGGING_LEVEL = 'level'
INI_LOGGING_LEVEL_DEFAULT = 'debug'

INI_WACOM = 'wacom'
INI_WACOM_TOOLS = 'tools'

INI_HISTORY = 'history'
INI_HISTORY_LEN = 10
INI_HISTORY_FILE = 'file'

INI_UI = 'ui'
INI_UI_EXPANDED = 'expanded'
INI_UI_EXPANDED_DEFAULT = 'True'

INI_UI_EXPANDED_TEXTURES = 'expanded_textures'
INI_UI_EXPANDED_TEXTURES_DEFAULT = 'True'

INI_UI_EXPANDED_SCREEN_ROTATION = 'expanded_screen_rotation'
INI_UI_EXPANDED_SCREEN_ROTATION_DEFAULT = 'True'

INI_UI_EXPANDED_IMAGE = 'expanded_image'
INI_UI_EXPANDED_IMAGE_DEFAULT = 'True'

INI_UI_EXPANDED_UNDO_HISTORY = 'expanded_undo_history'
INI_UI_EXPANDED_UNDO_HISTORY_DEFAULT = 'True'

INI_UI_EXPANDED_CURRENT_TOOL = 'expanded_current_tool'
INI_UI_EXPANDED_CURRENT_TOOL_DEFAULT = 'True'

INI_SIZE = 'size'
INI_SIZE_WIDTH = 'width'
INI_SIZE_WIDTH_DEFAULT = '1024'
INI_SIZE_HEIGHT = 'height'
INI_SIZE_HEIGHT_DEFAULT = '768'

# other variables
SKETCHBOOK_DIR = os.path.join(INI_DIR, 'sketchbooks')
SKETCHBOOK_FILE = 'sketchbook'

# widgets
LISTSTORE_FILE_HISTORY = 'liststore_file_history'
LISTSTORE_FILE_HISTORY_FULLNAME_IDX = 1
COMBOBOX_FILE_HISTORY = 'combobox_sketchbook_history'
EXPANDER_TEXTURES = 'expander_textures'
EXPANDER_SCREEN_ROTATION = 'expander_screen_rotation'
EXPANDER_IMAGE = 'expander_image'
EXPANDER_UNDO_HISTORY = 'expander_undo_history'
EXPANDER_CURRENT_TOOL = 'expander_current_tool'
WIDGET_WIDTH = 'adjustment_width'
WIDGET_HEIGHT = 'adjustment_height'
WIDGET_MAIN = 'box_main'
TOGGLEBUTTON_MAIN = 'togglebutton_main'
MAIN_WINDOW = 'MainWindow'

DEBUG_LEVELS = {'debug': logging.DEBUG,
	'info': logging.INFO,
	'warning': logging.WARNING,
	'error': logging.ERROR,
	'critical': logging.CRITICAL
	}

XSETWACOM = 'xsetwacom'
XRANDR = 'xrandr'
GIMP_START = ["gimp", "-b", "(python-fu-sketchbook 1)"]
GIMP_START_TIMEOUT = 10

DBUS_SERVER_SERVICE = 'org.borco.GimpSketchbook.Service'
DBUS_SERVER_INTERFACE = 'org.borco.GimpSketchbook.Interface'
DBUS_SERVER_PATH = '/org/borco/GimpSketchbook/Server'

def get_wacom_tools():
	ret = []
	output = subprocess.Popen([XSETWACOM, 'list', 'dev'], stdout = subprocess.PIPE).communicate()[0]
	for l in output.split('\n'):
		s = l.split()
		if len(s) == 2:
			ret.append(s[0])
		elif len(s) == 1 or len(s) > 2:
			logger.warning('Don\'t know how to handle xsetwacom device output: %s' % l)
	return ' '.join(ret)
	
def rotate_screen(mode):
	if mode == 'none':
		xrandr = 'normal'
		wacom = 'NONE'
	elif mode == 'cw':
		xrandr = 'right'
		wacom = 'CW'
	elif mode == 'ccw':
		xrandr = 'left'
		wacom = 'CCW'
	elif mode == 'half':
		xrandr = 'inverted'
		wacom = 'HALF'
	else:
		raise Exception('Unknown rotation mode: %s' % mode)
	
	subprocess.call([XRANDR, '--orientation', xrandr])
	tools = ini.get(INI_WACOM, INI_WACOM_TOOLS).split()
	logger.debug('Wacom configured tools: %s' % tools)
	for x in tools:
		subprocess.call([XSETWACOM, 'set', x, 'Rotate', wacom])

class Config(ConfigParser.SafeConfigParser):
	def __init__(self):
		ConfigParser.SafeConfigParser.__init__(self)
		self.read(INI_FILE)
	
	def getx(self, section, option, default):
		'''extended get - if the option is not set, return the default'''
		if self.has_option(section, option):
			return self.get(section, option)
		else:
			if self.has_section(section) == False:
				self.add_section(section)
			self.set(section, option, default)
			if logger: # logger might not be initialized yet!
				logger.debug('Initialized %s.%s: %s' % (section, option, default))
			return default

	def save(self):
		if os.path.exists(INI_DIR) == False:
			os.makedirs(INI_DIR)
		elif os.path.isdir(INI_DIR) == False:
			logger.warning('%s is in the way. Can\'t save the configuration.' % INI_DIR)
			return
		f = file(INI_FILE, 'w+')
		self.write(f)
		logger.info('Saved config in %s' % INI_FILE)
		f.close()

	def get_history(self):
		ret = []
		for x in range(INI_HISTORY_LEN):
			f = self.getx(INI_HISTORY, INI_HISTORY_FILE + str(x), '')
			if f != '':
				ret.append(f)
		return ret

class Client(object):
	def __init__(self):
		self.gimp_menu_paint_tool = None # dogtail proxy for gimp's "Tools>Paint Tools" menu
		self.gimp_menu_edit = None # dogtail proxy for gimp's "Edit" menu
		self.gimp_menu_file = None # dogtail proxy for gimp's "File" menu
		self.proxy = None
		self.current_sketchbook = None

		uifile = 'ui/sketchbook.ui'

		t = gtk.Builder()
		t.add_from_file(uifile)
		
		cell = gtk.CellRendererText()
		combo = t.get_object(COMBOBOX_FILE_HISTORY)
		combo.pack_start(cell, True)
		combo.add_attribute(cell, 'text', 2)
		
		t.connect_signals(self)
		
		t.get_object(MAIN_WINDOW).set_keep_above(True)

		self.wTree = t
		self.restore_ui()

		if not HAS_DOGTAIL:
			logger.warning('Dogtail missing! Some functionality will not be available!')
			for x in ['button_undo', 'button_redo', 'button_revert', 'button_pen', 'button_brush', 'button_eraser']:
				t.get_object(x).set_sensitive(False)

	def on_MainWindow_destroy(self, widget):
		try:
			if options.gimp_quit:
				if self.current_sketchbook is not None:
					self.gimp_proxy().SaveImage(self.current_sketchbook)
				self.gimp_proxy().QuitGimp(0) # don't force quit
		except dbus.DBusException, e:
			if e.get_dbus_name() != 'org.freedesktop.DBus.Error.ServiceUnknown':
				logger.error(e)
		except Exception, e:
			logger.error(e)
			traceback.print_exc()
		gtk.main_quit()

	def restore_ui(self):
		t = self.wTree

		# show the main window, if needed
		expanded = (ini.getx(INI_UI, INI_UI_EXPANDED, INI_UI_EXPANDED_DEFAULT) == 'True')
		t.get_object(TOGGLEBUTTON_MAIN).set_active(expanded)
		self.set_visibility(expanded)

		# restore visibility of the optional stuff
		expanded = (ini.getx(INI_UI, INI_UI_EXPANDED_TEXTURES,
			INI_UI_EXPANDED_TEXTURES_DEFAULT) == 'True')
		t.get_object(EXPANDER_TEXTURES).set_expanded(expanded)

		expanded = (ini.getx(INI_UI, INI_UI_EXPANDED_SCREEN_ROTATION,
			INI_UI_EXPANDED_SCREEN_ROTATION_DEFAULT) == 'True')
		t.get_object(EXPANDER_SCREEN_ROTATION).set_expanded(expanded)

		expanded = (ini.getx(INI_UI, INI_UI_EXPANDED_IMAGE,
			INI_UI_EXPANDED_IMAGE_DEFAULT) == 'True')
		t.get_object(EXPANDER_IMAGE).set_expanded(expanded)

		expanded = (ini.getx(INI_UI, INI_UI_EXPANDED_UNDO_HISTORY,
			INI_UI_EXPANDED_UNDO_HISTORY_DEFAULT) == 'True')
		t.get_object(EXPANDER_UNDO_HISTORY).set_expanded(expanded)

		expanded = (ini.getx(INI_UI, INI_UI_EXPANDED_CURRENT_TOOL,
			INI_UI_EXPANDED_CURRENT_TOOL_DEFAULT) == 'True')
		t.get_object(EXPANDER_CURRENT_TOOL).set_expanded(expanded)

		width = int(ini.getx(INI_SIZE, INI_SIZE_WIDTH, INI_SIZE_WIDTH_DEFAULT))
		t.get_object(WIDGET_WIDTH).set_value(width)

		height = int(ini.getx(INI_SIZE, INI_SIZE_HEIGHT, INI_SIZE_HEIGHT_DEFAULT))
		t.get_object(WIDGET_HEIGHT).set_value(height)

		for name in history[::-1]:
			t.get_object(LISTSTORE_FILE_HISTORY).prepend([0, name, os.path.basename(name)])
		t.get_object(COMBOBOX_FILE_HISTORY).set_active(0)
		
	def get_image_width(self):
		ret = int(self.wTree.get_object(WIDGET_WIDTH).get_value())
		return ret

	def get_image_height(self):
		ret = int(self.wTree.get_object(WIDGET_HEIGHT).get_value())
		return ret

	def store_ui(self):
		t = self.wTree

		expanded = str(t.get_object(TOGGLEBUTTON_MAIN).get_active())
		ini.set(INI_UI, INI_UI_EXPANDED, expanded)

		expanded = str(t.get_object(EXPANDER_TEXTURES).get_expanded())
		ini.set(INI_UI, INI_UI_EXPANDED_TEXTURES, expanded)

		expanded = str(t.get_object(EXPANDER_SCREEN_ROTATION).get_expanded())
		ini.set(INI_UI, INI_UI_EXPANDED_SCREEN_ROTATION, expanded)

		expanded = str(t.get_object(EXPANDER_IMAGE).get_expanded())
		ini.set(INI_UI, INI_UI_EXPANDED_IMAGE, expanded)

		expanded = str(t.get_object(EXPANDER_UNDO_HISTORY).get_expanded())
		ini.set(INI_UI, INI_UI_EXPANDED_UNDO_HISTORY, expanded)

		expanded = str(t.get_object(EXPANDER_CURRENT_TOOL).get_expanded())
		ini.set(INI_UI, INI_UI_EXPANDED_CURRENT_TOOL, expanded)

		ini.set(INI_SIZE, INI_SIZE_WIDTH, str(self.get_image_width()))

		ini.set(INI_SIZE, INI_SIZE_HEIGHT, str(self.get_image_height()))

		idx = 0
		for row in t.get_object('liststore_file_history'):
			name = row[LISTSTORE_FILE_HISTORY_FULLNAME_IDX]
			if os.path.exists(name):
				ini.set(INI_HISTORY, INI_HISTORY_FILE + str(idx), name)
				idx += 1
				if idx > INI_HISTORY_LEN:
					break

	def set_visibility(self, visible):
		if visible:
			self.wTree.get_object(WIDGET_MAIN).show()
		else:
			self.wTree.get_object(WIDGET_MAIN).hide()

	def on_togglebutton_sketchbook_toggled(self, widget):
		self.set_visibility(widget.get_active())

	def on_button_sketchbook_new_clicked(self, widget):
		def new_filename():
			idx = 1
			name_base = os.path.join(SKETCHBOOK_DIR, 
				SKETCHBOOK_FILE + datetime.date.today().strftime('-%Y%m%d-'))
			name = name_base + str(idx) + '.xcf'
			while os.path.exists(name):
				idx += 1
				name = name_base + str(idx) + '.xcf'
			return name

		t = self.wTree
		w = t.get_object('NewSketchbookWindow')
		if os.path.exists(SKETCHBOOK_DIR) == False:
			os.makedirs(SKETCHBOOK_DIR)
		w.set_current_folder(SKETCHBOOK_DIR)
		w.set_current_name(os.path.basename(new_filename()))
		ret = w.run()
		w.hide()
		if ret == 1: # button SAVE has ID 1
			name = w.get_filename()
			t.get_object(LISTSTORE_FILE_HISTORY).prepend([0, name, os.path.basename(name)])
			t.get_object(COMBOBOX_FILE_HISTORY).set_active(0)

	def start_gimp(self):
		'''start gimp using the current active file from combobox_sketchbook_history
		if there is no active item, an error is reported
		if the file is new (not on disk), the file is also saved
		'''
		
		# determine the current filename
		t = self.wTree
		idx = t.get_object(COMBOBOX_FILE_HISTORY).get_active()
		if idx == -1:
			logger.warning('No sketchbook selected. Create a new one or select an existing one!')
			return
		model = t.get_object(LISTSTORE_FILE_HISTORY)
		name = model[idx][LISTSTORE_FILE_HISTORY_FULLNAME_IDX]
		logger.debug('Using sketchbook: %s' % name)

		# move the current filename at the top of the list
		# the first item from row is 0, so need to change it!
		row = list(model[idx])
		del model[idx]
		model.prepend(row)
		t.get_object(COMBOBOX_FILE_HISTORY).set_active(0)
	
		port = ini.getx(INI_SERVER, INI_SERVER_PORT, INI_SERVER_PORT_DEFAULT)
		log_file = ini.getx(INI_SERVER, INI_SERVER_LOG, INI_SERVER_LOG_DEFAULT)

		subprocess.Popen(GIMP_START)

		time.sleep(GIMP_START_TIMEOUT)

		if HAS_DOGTAIL:
			gimp = dogtail.tree.root.application('gimp')
			self.gimp_menu_paint_tool = gimp.menu('Tools').menu('Paint Tools')
			self.gimp_menu_edit = gimp.menu('Edit')
			self.gimp_menu_file = gimp.menu('File')

		if os.path.exists(name):
			self.current_sketchbook = self.open_sketchbook(name)
		else:
			self.current_sketchbook = self.new_sketchbook(name)
		
		logger.debug("Current image: %s" % self.current_sketchbook)

	def gimp_proxy(self):
		bus = dbus.SessionBus()
		server = bus.get_object(DBUS_SERVER_SERVICE, DBUS_SERVER_PATH)
		return dbus.Interface(server, dbus_interface = DBUS_SERVER_INTERFACE)

	def open_sketchbook(self, name):
		return self.gimp_proxy().OpenSketchbook(name)

	def new_sketchbook(self, name):
		width = self.get_image_width()
		height = self.get_image_height()
		return self.gimp_proxy().NewSketchbook(name, int(width), int(height))

	def on_button_screen_none_clicked(self, widget):
		rotate_screen('none')

	def on_button_screen_cw_clicked(self, widget):
		rotate_screen('cw')

	def on_button_screen_ccw_clicked(self, widget):
		rotate_screen('ccw')

	def on_button_screen_half_clicked(self, widget):
		rotate_screen('half')

	def on_button_start_gimp_clicked(self, widget):
		self.start_gimp()

	def on_button_offset13_clicked(self, widget):
		self.gimp_proxy().Offset13(self.current_sketchbook)

	def on_button_offset31_clicked(self, widget):
		self.gimp_proxy().Offset31(self.current_sketchbook)

	def on_button_offset22_clicked(self, widget):
		self.gimp_proxy().Offset22(self.current_sketchbook)

	def on_button_offset2H_clicked(self, widget):
		self.gimp_proxy().Offset2H(self.current_sketchbook)

	def on_button_offset2V_clicked(self, widget):
		self.gimp_proxy().Offset2V(self.current_sketchbook)

	def on_button_page_new_clicked(self, widget):
		self.gimp_proxy().PageNew(self.current_sketchbook)

	def on_button_page_del_clicked(self, widget):
		self.gimp_proxy().PageDel(self.current_sketchbook)

	def on_button_page_next_clicked(self, widget):
		self.gimp_proxy().PageNext(self.current_sketchbook)

	def on_button_page_prev_clicked(self, widget):
		self.gimp_proxy().PagePrev(self.current_sketchbook)

	def on_button_layer_new_clicked(self, widget):
		self.gimp_proxy().LayerNew(self.current_sketchbook)

	def on_button_layer_del_clicked(self, widget):
		self.gimp_proxy().LayerDel(self.current_sketchbook)

	def on_button_layer_next_clicked(self, widget):
		self.gimp_proxy().LayerNext(self.current_sketchbook)

	def on_button_layer_prev_clicked(self, widget):
		self.gimp_proxy().LayerPrev(self.current_sketchbook)

	def on_button_image_rotation_left_clicked(self, widget):
		if self.current_sketchbook:
			self.gimp_proxy().RotateImage(self.current_sketchbook, 'left')

	def on_button_image_rotation_right_clicked(self, widget):
		if self.current_sketchbook:
			self.gimp_proxy().RotateImage(self.current_sketchbook, 'right')

	def on_button_image_rotation_180_clicked(self, widget):
		if self.current_sketchbook:
			self.gimp_proxy().RotateImage(self.current_sketchbook, '180')

	def on_button_eraser_clicked(self, widget):
		self.gimp_menu_paint_tool.menuItem('Eraser').click()

	def on_button_brush_clicked(self, widget):
		self.gimp_menu_paint_tool.menuItem('Paintbrush').click()

	def on_button_pen_clicked(self, widget):
		self.gimp_menu_paint_tool.menuItem('Pencil').click()

	def on_button_save_clicked(self, widget):
		if self.current_sketchbook is not None:
			self.gimp_proxy().SaveImage(self.current_sketchbook)

	def on_button_revert_clicked(self, widget):
		self.find_menu_item(self.gimp_menu_file, 'Revert').click()

	def on_button_redo_clicked(self, widget):
		self.find_menu_item(self.gimp_menu_edit, 'Redo').click()

	def on_button_undo_clicked(self, widget):
		self.find_menu_item(self.gimp_menu_edit, 'Undo').click()

	def find_menu_item(self, menu, prefix):
		for x in menu.findChildren(dogtail.predicate.GenericPredicate(roleName='menu item'), False):
			if x.name.startswith(prefix):
				return x
		return None

if __name__ == '__main__':
	# parse the command line
	parser = OptionParser()
	parser.add_option('--no-gimp-start', dest = 'gimp_start', action = 'store_false', default = True, help = 'don\'t start gimp on start')
	parser.add_option('--no-gimp-quit', dest = 'gimp_quit', action = 'store_false', default = True, help = 'don\'t quit gimp on close')
	(options, args) = parser.parse_args()
	
	ini = Config()

	logger = None

	level = DEBUG_LEVELS.get(ini.getx(INI_LOGGING, INI_LOGGING_LEVEL, INI_LOGGING_LEVEL_DEFAULT), 
		logging.NOTSET)
	logging.basicConfig(level = level, 
		format = '%(asctime)s [%(pathname)s:%(lineno)-3d] %(levelname)10s: %(message)s')

	logger = logging.getLogger("gimpsketchbook")

	# make sure the wacom tools are saved
	ini.getx(INI_WACOM, INI_WACOM_TOOLS, get_wacom_tools())

	# read the history
	history = ini.get_history()

	c = Client()
	if options.gimp_start:
		c.start_gimp()
	gtk.main()
	
	c.store_ui()
	ini.save()
