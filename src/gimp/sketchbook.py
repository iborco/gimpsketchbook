#! /usr/bin/python
# -*- coding: utf8 -*-

# GIMP Sketchbook - Transform GIMP into a sketchbook
# Copyright (C) 2009 Ionutz Borcoman, Tina Russell
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gobject

import dbus
import dbus.service
import dbus.mainloop.glib

import logging
import os

from gimpfu import *

AUTHOR = "Ionutz Borcoman <iborco@gmail.com>, Tina Russell"
COPYRIGHT = "AGPL http://www.fsf.org/licensing/licenses/agpl-3.0.html"
DATE = "2009"
MENU = "<Image>/Sketchbook"
TYPES = "RGB*, GRAY*"
ARGS = [
	(PF_IMAGE, "image", "Image", None),
	(PF_DRAWABLE, "drawable", "Drawable", None)
	]

DBUS_SERVER_SERVICE = 'org.borco.GimpSketchbook.Service'
DBUS_SERVER_INTERFACE = 'org.borco.GimpSketchbook.Interface'
DBUS_SERVER_PATH = '/org/borco/GimpSketchbook/Server'
DBUS_SERVER_EXCEPTION = 'org.borco.GimpSketchbook.Exception'

class Server(dbus.service.Object):
	def __init__(self, bus, path):
		self.bus = bus
		self.mainloop = gobject.MainLoop()
		self.prefix = bus.get_unique_name()
		dbus.service.Object.__init__(self, bus, path)

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='', out_signature='')
	def Exit(self):
		self.mainloop.quit()

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='s')
	def OpenSketchbook(self, name):
		img = pdb.gimp_file_load(name, name)
		pdb.gimp_display_new(img)
		pdb.gimp_image_clean_all(img)

		ret = img.name
		return ret

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='sii', out_signature='s')
	def NewSketchbook(self, name, width, height):
		img = pdb.gimp_image_new(width, height, RGB)
		layer = pdb.gimp_layer_new(img, width, height, RGB_IMAGE, "Page #1, Layer #1", 100, NORMAL_MODE)
		pdb.gimp_image_add_layer(img,layer,0)
		pdb.gimp_drawable_fill(layer,BACKGROUND_FILL)
		pdb.gimp_display_new(img)
		pdb.gimp_image_set_filename(img, name)
		pdb.gimp_file_save(img, layer, name, os.path.dirname(name))
		img.clean_all()

		ret = img.name
		return ret

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def Offset31(self, image):
		offset(self.get_image(image), 3, 1, 3, 1)

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def Offset13(self, image):
		offset(self.get_image(image), 1, 3, 1, 3)

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def Offset22(self, image):
		offset(self.get_image(image), 2, 2, 2, 2)

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def Offset2V(self, image):
		offset(self.get_image(image), 2, 0, 2, 2)

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def Offset2H(self, image):
		offset(self.get_image(image), 2, 2, 2, 0)
	
	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def LayerNew(self, image):
		img = self.get_image(image)
		
		y = gimp.Layer(img, "Page #" + str(get_page_number(img.active_layer)) + ", Layer #1", img.width, img.height, RGBA_IMAGE, 100, NORMAL_MODE)
		
		pos = get_position(img, img.active_layer)
		img.add_layer(y, pos)
	
	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def LayerDel(self, image):
		img = self.get_image(image)
		
		current_page = get_page_number(img.active_layer)
		layers_in_page = get_layers_in_page(img, current_page)
		
		if len(layers_in_page) == 1:
			self.PageDel(image)
		else:
			layer_to_delete = img.active_layer
			current_layer_number = get_current_layer_from_list(img, layers_in_page)
			del layers_in_page[current_layer_number]
			try:
				img.active_layer = layers_in_page[current_layer_number]
			except IndexError:
				img.active_layer = layers_in_page[-1]
			img.remove_layer(layer_to_delete)
			gimp.displays_flush()
	
	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def LayerPrev(self, image):
		img = self.get_image(image)
		
		current_page = get_page_number(img.active_layer)
		layers_in_page = get_layers_in_page(img, current_page)
		pos_in_page = get_current_layer_from_list(img, layers_in_page)
		img.active_layer = layers_in_page[(pos_in_page-1)%len(layers_in_page)]
	
	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def LayerNext(self, image):
		img = self.get_image(image)
		
		current_page = get_page_number(img.active_layer)
		layers_in_page = get_layers_in_page(img, current_page)
		pos_in_page = get_current_layer_from_list(img, layers_in_page)
		img.active_layer = layers_in_page[(pos_in_page+1)%len(layers_in_page)]

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def PageNew(self, image):
		img = self.get_image(image)
		
		# determine the current page number
		try:
			current_page = get_page_number(img.active_layer)
		except AttributeError:
			# in case there are currently no pages
			current_page = -1
		
		# determine the new page number, which will be the first unoccupied page
		# number that is higher than the current one
		new_page_number = current_page + 1
		if new_page_number < 1:
			new_page_number = 1
		while get_layers_in_page(img, new_page_number) != []:
			new_page_number += 1
		
		# create and set the page
		y = gimp.Layer(img, "Page #" + str(new_page_number) + ", Layer #1", img.width, img.height, RGB, 100, NORMAL_MODE)
		y.fill(BACKGROUND_FILL)
		
		# adds the page after the top layer of the current page
		if current_page != -1:
			current_page_top_layer = get_layers_in_page(img, current_page)[-1]
			pos = get_position(img, current_page_top_layer)
		else:
			pos = 0
		img.add_layer(y, pos)
		set_active_page(img, new_page_number)

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def PageDel(self, image):
		img = self.get_image(image)

		current_page = get_page_number(img.active_layer)
		page_list = get_page_list(img)
		current_page_index = page_list.index(current_page)
		page_list.remove(current_page)
		
		for y in get_layers_in_page(img, current_page):
			img.remove_layer(y)
		if len(img.layers) == 0:
			self.PageNew(image)
		else:
			try:
				next_page = page_list[current_page_index]
			except IndexError:
				next_page = page_list[-1]
			set_active_page(img, next_page, 'next')

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def PageNext(self, image):
		img = self.get_image(image)

		current_page = get_page_number(img.active_layer)
		page_list = get_page_list(img)
		current_page_index = page_list.index(current_page)
		try:
			next_page = page_list[current_page_index + 1]
		except IndexError:
			return None
		set_active_page(img, next_page, 'next')

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def PagePrev(self, image):
		img = self.get_image(image)

		current_page = get_page_number(img.active_layer)
		page_list = get_page_list(img)
		current_page_index = page_list.index(current_page)
		if current_page_index == 0:
			return None
		else:
			prev_page = page_list[current_page_index - 1]
		set_active_page(img, prev_page, 'prev')

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='ss', out_signature='')
	def RotateImage(self, image, mode):
		img = self.get_image(image)
		if mode == 'left':
			m = ROTATE_270
		elif mode == 'right':
			m = ROTATE_90
		elif mode == '180':
			m = ROTATE_180
		else:
			print 'Unknown rotation mode:', m
			return # unknown mode
		pdb.gimp_image_rotate(img, m)
		gimp.displays_flush()

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='s', out_signature='')
	def SaveImage(self, image):
		img = self.get_image(image)
		layer = img.active_layer
		pdb.gimp_file_save(img, layer, img.filename, os.path.dirname(img.filename))
		img.clean_all()

	@dbus.service.method(DBUS_SERVER_INTERFACE, in_signature='i', out_signature='')
	def QuitGimp(self, force):
		pdb.gimp_quit(force)

	def run(self):
		self.mainloop.run()

	def get_image(self, image):
		ret = None
		for x in gimp.image_list():
			if x.name == image:
				ret = x
		if ret is None:
			raise ServerException("Image %s is not opened!" % image)
		return ret

class ServerException(dbus.DBusException):
	_dbus_error_name = DBUS_SERVER_EXCEPTION

def offset(timage, x1, x2, y1, y2):
	img = timage

	w = img.width
	h = img.height
	
	w1 = int(w * x1 / float(x1 + x2))
	h1 = int(h * y1 / float(y1 + y2))

	l = img.active_layer
	pdb.gimp_drawable_offset(l, 1, 1, w1, h1)
	gimp.displays_flush()

def get_position(img, layer):
	ret = 0
	for l in img.layers:
		if l == layer:
			return ret
		ret += 1
	return ret

def set_active_page(img, pos, alt_dir='next'):
	'''Make all layers of the given page visible, and make all others invisible.
	Make the first layer of the page active.'''
	
	# ensure the given page number is valid
	h = highest_page_number(img)
	if pos > h:
		pos = h
	if pos < 0:
		pos = 0
	
	# Obtain the layers of the page. If there are no layers with that page
	# number, try a page number one higher.
	active_layers = []
	while active_layers == []:
		if (pos > h) or (pos < 0):
			return None
		active_layers, inactive_layers = get_layers_in_page(img, pos, 1)
		if (alt_dir == 'prev') or (alt_dir == True):
			pos -= 1
		else:
			pos += 1
	img.active_layer = active_layers[0]
	
	# make the relevant layers visible and the irrelevant ones invisible
	for y in active_layers:
		y.visible = 1
	for y in inactive_layers:
		y.visible = 0
	
	gimp.displays_flush()
	
def get_page_number(layer):
	'''Returns the page number found in “Page #_, Layer #_”.'''
	
	num_index = layer.name.find('#')+1
	comma_index = layer.name.find(',', num_index)
	try:
		return int(layer.name[num_index:comma_index])
	except ValueError:
		# All layers without the standard name structure are considered
		# one big “page 0.” TODO(?): treat each such layer as its own page
		return 0

def get_layer_number(layer):
	'''Returns the layer number found in “Page #_, Layer #_”.'''
	
	num_index = layer.name.find('#')+1
	num2_index = layer.name.find('#', num_index)+1
	try:
		return int(layer.name[num2_index:])
	except ValueError:
		return 0

def get_page_list(img):
	'''Returns a list of numbers, corresponding to every page found in the
	image.'''
	
	layer_list = img.layers
	layer_list.reverse()
	page_list = []
	
	# Walk through each layer, getting each layer’s page number along the way.
	# If the page number is not present in the list, add it.
	# Each page number will only be added to the list once; the final list
	# will be in the order of each page’s lowest layer.
	for y in layer_list:
		p = get_page_number(y)
		if p not in page_list:
			page_list.append(p)
	
	print(page_list)
	return page_list

def get_layers_in_page(img, page, return_others=0):
	'''Returns all layers with a certain page number, in a list. The layers
	will be ordered as they appear in GIMP, though with “lower” layers given
	lower indices and “higher” layers given higher indices.
	
	If “return_others” is true, then it will return a tuple of two lists:
	the first will be the list of layers with a certain page number, and the
	second will be a list of all layers without that page number.'''
	
	layers_in_page = []
	other_layers = []
	
	for x in img.layers:
		if get_page_number(x) == page:
			layers_in_page.append(x)
		elif return_others:
			other_layers.append(x)
	layers_in_page.reverse()
	if return_others:
		other_layers.reverse()
		return (layers_in_page, other_layers)
	else:		
		return layers_in_page

def get_current_layer_from_list(img, layers_in_page):
	'''Returns the index of the current layer as it is found in the given
	list.'''
	for x in range(len(layers_in_page)):
		if layers_in_page[x] == img.active_layer:
			return x
	return -1

def highest_page_number(img):
	'''Finds the highest occupied page number. Currently, this is only used
	to validate variables.'''
	page_number_list = []
	for x in img.layers:
		page_number_list.append(get_page_number(x))
	return max(page_number_list)

def sketchbook():
	print 'python-fu-sketchbook started...'
	dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	name = dbus.service.BusName(DBUS_SERVER_SERVICE, bus)
	server = Server(bus, DBUS_SERVER_PATH)
	server.run()
	print 'python-fu-sketchbook finished...'

# gimp registering
register(
	"sketchbook",
	"Sketchbook Server",
	"Sketchbook Server",
	AUTHOR,
	COPYRIGHT,
	DATE,
	"Sketchbook Server",
	"",
	[],
	[],
	sketchbook,
	menu = MENU
	)

main()
